[
    [
        "0798ac4a-4d4f-4648-95f0-12676b3411d5",
        "Miljøhuset"
    ],
    [
        "0adc134f-de45-4557-9a0f-444341ceda22",
        "SF auto"
    ],
    [
        "0b6970d1-3da7-494f-92ba-173be9b937d9",
        "Namsskogan Oms"
    ],
    [
        "11c350e3-ae67-4033-9a85-ac20a7a316f8",
        "Modumheimen Sykehjem"
    ],
    [
        "1ed23da1-2c9e-4d98-b4fa-9caac12db4f1",
        "Bergen Storsenter"
    ],
    [
        "3022871f-f51c-46ae-b7c8-44ffeee8135e",
        "ST hansgt 1"
    ],
    [
        "302e1049-ce9e-44ff-b574-07a4cb72b74b",
        "Centrum gården"
    ],
    [
        "4d1a7649-8274-479e-936b-705fde36b2cd",
        "AB Boken"
    ],
    [
        "55f9b5e9-f65b-4cd2-b6ae-a83f7dc3f5c6",
        "Teater Ibsen"
    ],
    [
        "5b61f0fb-00d5-4472-99c7-86fce81fd8a2",
        "Ahlbergs Bil"
    ],
    [
        "5dd96f6c-9e36-41ec-9829-78bdda2af3e1",
        "Lietorvet"
    ],
    [
        "692c559f-d8e4-42be-a8da-e7eadefb9e8d",
        "AMFI Kirkenes"
    ],
    [
        "75735dc9-108c-45ba-88d3-27867f0f6aa7",
        "Dronninggata 15"
    ],
    [
        "7ba9390d-e74b-4be0-b320-de62d5439f98",
        "Trollheim"
    ],
    [
        "7d4fedc3-8cc6-4e79-88f8-63f437e51a33",
        "Bodø Spektrum"
    ],
    [
        "818a0284-684d-490e-8cdf-3b3d35abcc08",
        "AMFI Svolvær"
    ],
    [
        "82263d07-6937-4323-9309-3516e83c2ecc",
        "Maxi Kristianstad"
    ],
    [
        "84bd8fda-fbf5-42a8-a187-aeee7ac4d688",
        "Asker VGS"
    ],
    [
        "892cc3fc-f07f-41b5-b65f-ab340b09b463",
        "TOBB"
    ],
    [
        "975f417d-9dc2-4642-a895-fa3a266abef8",
        "E.C. Dahls HIST"
    ],
    [
        "9a9f6102-f775-4e81-9d0f-256dffc6cb79",
        "Bodø S"
    ],
    [
        "9b43cd6c-148e-4943-9f08-3f18239dfefd",
        "AMFI Namsos"
    ],
    [
        "9f608c27-b63d-4123-a240-e09359ca6816",
        "Medborgarhuset"
    ],
    [
        "b0538a6d-5f99-4ddc-8194-b271d80f3df3",
        "GK Tønsberg"
    ],
    [
        "ba027f9e-bae1-4c8d-aade-f8ec5dda9042",
        "Trondheim S"
    ],
    [
        "ca8fa4f1-c87d-4627-b19a-99a861bbe84b",
        "Sandetun"
    ],
    [
        "d399f9ef-b17e-4fb2-a2ae-3e6eaa7ded16",
        "Bangeløkka"
    ],
    [
        "dbb9a570-f3e4-400d-98b7-eda53a9ab789",
        "Øya VGS"
    ],
    [
        "de613539-d7d7-4278-a0c5-98861172e8c7",
        "Vestre Strand gt 21"
    ],
    [
        "0065ec76-4e36-4f52-8bd0-19260e97001d",
        "Klostergata 30"
    ]
]