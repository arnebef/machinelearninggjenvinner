import optparse
import numpy as np
import json
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plot
import matplotlib.dates
from matplotlib.mlab import griddata
from matplotlib.colors import ListedColormap
from scipy.stats import gaussian_kde
import datetime
import urllib.request
import hashlib
import os.path
import pickle
from gjenvinner import *
from collections import OrderedDict
import concurrent.futures
from multiprocessing import Process, Queue, Pool


TIME_FORMAT="%Y-%m-%d %H:%M:%S"
LIMIT=80
URL_TEMPLATE="https://historiangk.piscada.cloud/%s/timeseries/%s?from=2018-01-01T00:00"

class TimeSeries:
    """Represents a 2-dimensional time-series value."""
    def __init__(list_of_tuples=[], names=[]):
        columns = names
        array = np.empty(1,len(list_of_tuples),dtype=np.float64)
        timestamps = [ts[0] for ts in list_of_tuples]
        array = np.reshape([ts[1] for ts in list_of_tuples],dtype=np.float64)
            
       
    @staticmethod
    def merge(list_of_ts):
        """ Merges 2 timeseries together. Unknown data is set to be NaN """
        new_ts = TimeSeries()
        new_ts.columns = zip(lambda x,y: x+y,[])
        new_ts.array = np.empty(len(self.columns),0)
        

def time_transform(d,func):
    '''
    Function that takes in a dict of lists of tuples (timestamp,value). Each list
    represents a sensor with the name as the key in the dict.
    Each tuple signifies the value the sensor has at the measurment until the
    next tuple for the dict. If the sensor has yet to receive the value, the value will
    be nil.
    For each timestamp, func will be called and the result added into a table with tuple 
    (timestamp,value). To suppress adding the value to the table, have func return nil.
    '''
    timestamps = []
    for name,values in d.items():
        #print(name)
        timestamps += [(timestamp,name,value) for timestamp,value in values]
    
    states = {}
    for key,_ in d.items():
        states[key] = None

    timestamps.sort()
    i = 0
    results = []
    while i < len(timestamps):
        t,_,_ = timestamps[i]
        while i < len(timestamps) and t == timestamps[i][0]:
            t,name,value = timestamps[i]
            states[name] = value
            i += 1
        #print(t)
        v = func(states)
        if v != None:
            results.append((t,v))
    return results


def transform(gjenvinning, paadrag, limit):
    """Transforms 'gjenvinning' and 'paadrag' into a combined time 
    where the combined values are filtered to when the 'paadrag' is
    higher than 'limit'. For those time series, the combined value
    should be set to 0. Inputs are list of (timestamp,value) where 
    the lists are sorted on timestamp."""
    timestamps = [x[0] for x in gjenvinning] + [x[0] for x in paadrag]
    timestamps.sort()
    gjenvinning.sort()
    paadrag.sort()

    p = 0
    v = float('nan')
    t = timestamps[0]
    gi = 0
    pi = 0
    results = []
    for newt in timestamps:
        #print(gjenvinning[gi])
        while gi < len(gjenvinning)-1 and gjenvinning[gi+1][0] <= newt:
            gi += 1
            #print(gi,gjenvinning[gi])
        #print(paadrag[pi])
        while pi < len(paadrag)-1 and paadrag[pi+1][0] <= newt:
            pi += 1
            #print(pi,paadrag[pi])
        newp = paadrag[pi][1]
        newv = gjenvinning[gi][1]
        #print(t,gi,newt,v,p,newp)
        if p < LIMIT:
            results.append((t,float('nan')))
        else: 
            results.append((t,v))
            results.append((newt,v))
            if newp < LIMIT:
                results.append((t,float('nan')))
        p = newp
        v = newv
        t = newt
    #print(results)
    #for t,v in results:
    #    print(t,v)
    return results

def gjenvinning(states):
    #print(states)
    v = states['virkningsgrad']
    p = states['padrag']
    if p == None or p == None:
        return None
    if p < LIMIT:
        return float('nan')
    else:
        return v

def fetch_json(uid, tag):
    url = URL_TEMPLATE % (uid,tag)
    hashed_name = ".cache." + hashlib.sha256(url.encode('utf-8')).hexdigest()
    if not os.path.isfile(hashed_name):
        with urllib.request.urlopen(URL_TEMPLATE % (uid,tag)) as u:
            print(url)
            j = json.load(u)
            result = [(datetime.datetime.strptime(x["ts"],TIME_FORMAT),x["v"]) for x in j]
        with open(hashed_name,"wb") as f:
            pickle.dump(result,f)
    with open(hashed_name,"rb") as f:
        result = pickle.load(f)
    return result

#E_{gg} = (R_{in\_after} - R_{in\_before})*\frac{m_{luft}}{s}*(t_i - t_{i-1})*C_{p\_luft}
#m_luft/s = luft_volum/s*trykk/(R_specific*(R_in_after + 273.15))
def energi(states):
    if states["rinntak_inn"] == None or \
        states["rinntak_ut"] == None or \
        states["trykk"] == None or \
        states["luftmengde"] == None:
        return None
    C_p = 1.005 # kj/(kgK)
    R_specific = 287.058 #J/(kgK)
    T_k = states["rinntak_ut"]+273.15 #Kelvin
    temp_diff = states["rinntak_ut"] - states["rinntak_inn"] #K
    p = (1000*states["trykk"])/(R_specific*T_k) 
    m_luft_per_hour = states["luftmengde"]*p
    effekt_kjph = temp_diff*m_luft_per_hour*C_p #kj/h
    effekt_kw = (1.0/3600.0)*effekt_kjph #kwatt
    return effekt_kw

def create_graph(site,t,desc):
    #print(site)
    for v in desc['values']:
        if v not in t.keys():
            return
    values = {}
    for v in desc['values']:
        values[v] = fetch_json(site['uid'],t[v])
    values[desc["out_name"]] = time_transform(values,desc["func"])
    #print("Plotted %s %s" % (site["name"],desc["out_name"]))

    #Now actually do the plotting
    _,ax = plot.subplots()
    for s in desc["show"]:
        ax.plot([x[0] for x in values[s]],[x[1] for x in values[s]])
    if "show_twin" in desc:
        ax2 = ax.twinx()
        for s in desc["show_twin"]:
            ax2.plot([x[0] for x in values[s]],[x[1] for x in values[s]])
    plot.savefig("%s_%s.png" % (site["name"],desc["out_name"]))
    plot.clf()


def plot_histogram(x,y,site,system,xval,yval,ymin,ymax,xmin,xmax):

    cmap = plot.cm.Greens
    my_cmap = cmap(np.arange(cmap.N))
    my_cmap[:,-1] = np.linspace(0.8,1,cmap.N)
    my_cmap[:,-1][0] = 0
    my_cmap = ListedColormap(my_cmap)

    print(site)
    fig = plot.figure(figsize=(10,10),dpi=300)
    plot.hist2d(x,y,range=[[xmin,xmax],[ymin,ymax]],bins=200, label=site,cmap=my_cmap)
    plot.gca().set_xlim([xmin,xmax])
    plot.gca().set_xlabel(str(xval))
    plot.gca().set_ylim(ymin,ymax)
    plot.gca().set_ylabel(str(yval))
    handles,labels = plot.gca().get_legend_handles_labels()
    plot.gca().legend(handles,labels)
    plot.savefig("%s_%s_%s_%s.png" % (site,str(system),xval,yval))
    plot.close()

def create_plot(sites,desc):
    p = Pool(maxtasksperchild=1)
    for xval,yval,ymin,ymax,xmin,xmax in desc['show']:
        
        #print(sites)
        for (site,system) in sites.keys():
            
            #print(site,sensors)
            #print(site,gjenvinnerTags(sensors))
            t = gjenvinnerTags(sites,site,system)
            print(site,system)
            #t = site["tags"][0]
            if not xval in t or not yval in t:
                print(t)
                if system.systemtype == "360":
                    for sensor in sensors:
                        print(sensor)
                continue
            print(site,system,t[xval].tag,t[yval].tag)
            x_time = fetch_json(uids[site],t[xval].tag)
            y_time = fetch_json(uids[site],t[yval].tag)

            x = []
            y = []
            def combine(d):
                if d["x"] == None or d["y"] == None:
                    return
                x.append(d["x"])
                y.append(d["y"])
                return

            time_transform({"x":x_time,"y":y_time}, combine)
            if len(x) == 0 or len(y) == 0:
                print(site + " is empty. Continuing on next")
                continue

            X,Y = np.mgrid[min(x):max(x):100j, :max(y):100j] 
            positions = np.vstack([X.ravel(),Y.ravel()])
            values = np.vstack([x,y])
            #kernel = gaussian_kde(values)
            #Z = np.reshape(kernel(positions).T,X.shape)
            #ax.imshow(np.rot90(Z), cmap=plot.cm.gist_earth_r,aspect='auto',extent=[min(x),max(x),min(y),max(y)])
            p.apply_async(func=plot_histogram,args=(x,y,site,system,xval,yval,ymin,ymax,xmin,xmax))
            # plot.hist2d(x,y,range=[[min(x),max(x)],[ymin,ymax]],bins=200, label=site)
            # plot.gca().set_xlim([min(x),max(min(x)+1,max(x))])
            # plot.gca().set_xlabel(str(xval))
            # plot.gca().set_ylim(ymin,ymax)
            # plot.gca().set_ylabel(str(yval))
            # handles,labels = plot.gca().get_legend_handles_labels()
            # plot.gca().legend(handles,labels)
            # plot.savefig("%s_%s_%s_%s.png" % (site,str(system),xval,yval))
            # plot.close()
    p.close()
    p.join()


energi_graph = {
       "values" : ["rinntak_inn","rinntak_ut","trykk","luftmengde"],
       "out_name" : "energi",
       "func" : energi,
       "show" : ["energi"],
       "show_twin" : ["rinntak_inn","rinntak_ut"]
}

virkningsgrad_graph = {
       "values" : ["padrag","virkningsgrad",],
       "func" : gjenvinning,
       "out_name" : "vut",
       "show" : ["vut"],
}

temp_plot = {
        "show" : [(GjenvinnerSensor.TEMP_INNLUFT,GjenvinnerSensor.VIRKNINGSGRAD,0,100,-20,20),
                    #("rinntak_inn","trykk",0,150),("rinntak_inn","luftmengde",2000,8000),("rinntak_inn","rinntak_ut",0,30), ("rinntak_inn","padrag",0,100)]
                ]
}

def create_graphs(site,t):
    pass
#    create_graph(site,t,virkningsgrad_graph)
#    create_graph(site,t,energi_graph)
#    create_plot(site,t,temp_plot)

parser = optparse.OptionParser()
_,args  = parser.parse_args()

assert len(args) == 1
sites_file = args[0]

with open(sites_file, "r") as f:
    sensors = json.load(f)

    sites = {}
    for sensor in sensors:
        #print(sensor)
        s = parseTag(sensor["result"]["Name"])

        if s == None:
            #print("Failed to parse sensor %s" % sensor)
            continue
        if not (sensor["name"],s.system) in sites:
            sites[sensor["name"],s.system] = []
        sites[(sensor["name"],s.system)].append(s)

with open("uidNames.js","r") as f:
    uidJson = json.load(f)
    uids = {}
    for entry in uidJson:
        uids[entry[1]] = entry[0]

create_plot(sites,temp_plot)
    
#modum = sites[0]
#mtags = modum['tags'][0]
#virkningsgrad = fetch_json(modum['uid'],mtags['virkningsgrad'])
#temp_inn = fetch_json(modum['uid'],mtags['rinntak_inn'])
#temp_ut = fetch_json(modum['uid'],mtags['rinntak_ut'])

#plot.plot([x[0] for x in temp_inn],[x[1] for x in temp_inn])
#plot.plot([x[0] for x in temp_ut],[x[1] for x in temp_ut])
#plot.plot([x[0] for x in virkningsgrad],[x[1] for x in virkningsgrad])
#plot.show()

'''
for site in sites:
    #Code to dump out combined virkningsgrad
    for t in site['tags']:
        virkningsgrad = fetch_json(site['uid'],t['virkningsgrad'])
        padrag = fetch_json(site['uid'],t['padrag'])
        combined = transform(virkningsgrad,padrag,LIMIT)
        #combined= list(filter(lambda x: x[1] > 0.0,combined))
        #plot.plot([x[0] for x in virkningsgrad],[x[1] for x in virkningsgrad],drawstyle="steps-post",linewidth=0.4)
        plot.plot([x[0] for x in padrag],[x[1] for x in padrag],drawstyle="steps-post",linewidth=0.4, label="Paadrag")
        plot.plot([x[0] for x in combined],[x[1] for x in combined], label="Effektivitet")
        plot.legend(bbox_to_anchor=(1,0.0),mode="expand")
        ax = plot.gca()
        ax.set_ylim([-10,110])
        ax.xaxis.set_major_locator(matplotlib.dates.WeekdayLocator())
        ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%d/%m'))
        #ax.xaxis.set_minor_formatter(matplotlib.dates.DateFormatter('%d'))
        ax.xaxis.set_minor_locator(matplotlib.dates.DayLocator())
        plot.setp(ax.get_xticklabels(),rotation=45,horizontalalignment='right')
        plot.savefig("%s_%s.png" % (site["name"],t["virkningsgrad"]))
        plot.clf()
'''
#combined = transform(parse_json(gjenvinning_file),parse_json(paadrag_file), LIMIT)

#print(combined)

