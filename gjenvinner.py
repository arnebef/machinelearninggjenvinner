from enum import Enum
import re
class GjenvinnerSensor(Enum):
    TEMP_INNLUFT = 0
    TEMP_FRALUFT = 1
    TEMP_INN_ETTER_VARMEGJENVINNER = 2
    TEMP_UT_ETTER_VARMEGJENVINNER = 3
    PADRAG = 4
    VIRKNINGSGRAD = 5


class SensorTag():
    def __init__(self,componenttype,componentnr,parameter):
        self.componenttype = componenttype
        self.componentnr = int(componentnr)
        self.parameter = parameter

    def __eq__(self,other):
        return (self.componenttype == other.componenttype) and \
                (self.componentnr == other.componentnr) and\
                (self.parameter == other.parameter)

    def __str__(self):
        return self.componenttype + str(self.componentnr) + self.parameter

class System():
    def __init__(self,systemtype,systemnr):
        self.systemnr = systemnr
        self.systemtype = systemtype

    def __eq__(self,other):
        return self.systemnr == other.systemnr and self.systemtype == other.systemtype

    def __hash__(self):
        return hash(str(self))

    def __str__(self):
        return self.systemtype + self.systemnr
    

class Sensor():
    def __init__(self,system,sensor,tag):
        self.system = system
        self.sensor = sensor
        self.tag = tag

    def __str__(self):
        return str(self.system) + " " + self.tag

        
delim = '[\.\_\- ]*'
end = '(?P<suffix>$|[^A-Z].*)'
system = '^(?P<prefix>.*[^\d]|)(?P<systemtype>\d\d\d)(?P<systemdelim>0*' + delim + '0*)(?P<systemnr>\d+)' + delim
component = '(?P<componenttype>[A-Z][A-Z])' + '(?P<componentnr>\d\d\d?)[A-Z]?' + delim + '(?P<parameter>[A-Z]*)' + end
tagregex = re.compile(system + component)
componentregex = re.compile('(?P<componenttype>[A-Z][A-Z])' + '(?P<componentnr>\d\d\d?)[A-Z]?')
systemregex = re.compile('^(?P<prefix>.*[^\d]|)(?P<systemtype>\d\d\d)(?P<systemdelim>0*' + delim + '0*)(?P<systemnr>\d+)' + delim)


def parseTag(tag):
    result = tagregex.search(tag)
    if result:
        systemtype = result["systemtype"]
        systemnr = result["systemnr"]
        componenttype = result["componenttype"]
        componentnr = result["componentnr"]
        parameter = result["parameter"]
        sensor = Sensor(System(systemtype,systemnr),SensorTag(componenttype, componentnr, parameter),tag)
        return sensor
    return None




gjenvinnersensors = { GjenvinnerSensor.TEMP_INNLUFT:[
            SensorTag("RT", 900, "MV"),
            SensorTag("RT", 901, "MV"),
            #SensorTag("RT", 401, "MV"),
            SensorTag("RT", 90, "MV"),
            SensorTag("RT", 900, ""),
            #SensorTag("RT", 41, "MV"),
            SensorTag("RT", 401, "PV"),
            SensorTag("RT", 401, "MV"),],
    GjenvinnerSensor.TEMP_FRALUFT:[
            SensorTag("RT", 500, "MV"),
            SensorTag("RT", 501, "MV"),
            SensorTag("RT", 54, "MV"),
            SensorTag("RT", 50, "MV"),
            SensorTag("RT", 500, "")],
    GjenvinnerSensor.TEMP_INN_ETTER_VARMEGJENVINNER:[
            SensorTag("RT", 410, "MV"),
            SensorTag("RT", 402, "MV"),
            SensorTag("RT", 41, "MV"),
            SensorTag("RT", 41, "PV"),
            SensorTag("RT", 410, "")],
    GjenvinnerSensor.TEMP_UT_ETTER_VARMEGJENVINNER:[
            SensorTag("RT", 504, "MV"),
            SensorTag("RT", 502, "MV"),
            SensorTag("RT", 54, "MV"),
            SensorTag("RT", 51, "PV"),
            SensorTag("RT", 51, "MV"),
            SensorTag("RT", 504, "")],
    GjenvinnerSensor.PADRAG:[
            SensorTag("LX", 471, "C"),
            SensorTag("LX", 47, "C"),
            SensorTag("LR", 471, "C"),
            SensorTag("LR", 471, "CL"),
            SensorTag("LX", 1, "C"),
            SensorTag("LR", 403, "C"),
            SensorTag("LX", 403, "C"),
            SensorTag("LR", 1, "C"),
            SensorTag("LR", 41, "C"),
            SensorTag("KA", 401, "C")], #This is very wrong
    GjenvinnerSensor.VIRKNINGSGRAD:[
            SensorTag("LX", 471, "KV"),
            SensorTag("LX", 47, "KV"),
            SensorTag("LR", 471, "KV"),
            SensorTag("LX", 403, "KV"),
            SensorTag("LR", 403, "KV"),
            SensorTag("LX", 1, "KV"),
            SensorTag("LR", 1, "KV"),
            SensorTag("LX", 41, "KV"),
            SensorTag("KA", 401, "KV")] #This is very wrong
}

def gjenvinnerTags(sites,site,system):
    sensors = sites[(site,system)]
    result = {}
    for gjenvinnerTag,gjenvinnerList in gjenvinnersensors.items():
        for gjenvinnerSensor in gjenvinnerList:
            for sensor in sensors:
                if gjenvinnerSensor == sensor.sensor:
                    result[gjenvinnerTag] = sensor
            if gjenvinnerTag in result.keys():
                break 

    #Treat TEMP_INNLUFT specially, it can be sourced from other systems at the same site if it is missing
    if GjenvinnerSensor.TEMP_INNLUFT in result.keys():
        for gjenvinnerSensor in gjenvinnersensors[GjenvinnerSensor.TEMP_INNLUFT]:
            for (tsite,tsystem),sensors in sites.items():
                if (tsite,tsystem) == (site,system):
                    for sensor in sensors:
                        if sensor.sensor == gjenvinnerSensor:
                            result[GjenvinnerSensor.TEMP_INNLUFT] = sensor

    return result